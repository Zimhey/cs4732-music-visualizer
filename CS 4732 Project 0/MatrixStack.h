#pragma once

#include <glm\mat4x4.hpp>

class MatrixStack
{
public:
	MatrixStack();
	~MatrixStack();
	void push(glm::mat4 mat);
	glm::mat4 pop();
	glm::mat4 resultMat() const;
	bool empty() const;

private:
	glm::mat4* mats;
	int numMats;
	int used;
};

