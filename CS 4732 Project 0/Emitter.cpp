#include "Emitter.h"
#include <glm\gtx\euler_angles.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>

Emitter::Emitter(int numParticles) : Emitter(numParticles, glm::vec3())
{
}

Emitter::Emitter(int numParticles, glm::vec3 position)
{
	this->numParticles = numParticles;
	particles = new Particle[numParticles];

	this->position = position;
	this->velocity = glm::vec3();
	this->acceleration = glm::vec3();
	spawnRadius = 0.f;

	maxLife = minLife = 0.f;
	maxInitParticleVel = minInitParticleVel = 0.f;
	maxInitParticleAccel = minInitParticleAccel = 0.f;
	particleVelConeAngle = particleAccelConeAngle = 0.f;
	particleVelDir = particleAccelDir = glm::vec3();
}


Emitter::~Emitter()
{
	if (particles != NULL)
		delete[] particles;
}

void Emitter::setPosition(glm::vec3 position)
{
	this->position = position;
}

glm::vec3 Emitter::getPosition() const
{
	return position;
}

void Emitter::setVelocity(glm::vec3 velocity)
{
	this->velocity = velocity;
}

glm::vec3 Emitter::getVelocity() const
{
	return velocity;
}

void Emitter::setAcceleration(glm::vec3 acceleration)
{
	this->acceleration = acceleration;
}

glm::vec3 Emitter::getAcceleration() const
{
	return acceleration;
}

void Emitter::setSpawnRadius(float radius)
{
	this->spawnRadius = radius;
}

float Emitter::getSpawnRadius() const
{
	return spawnRadius;
}

void Emitter::setShader(sf::Shader & shader)
{
	this->shader = &shader;
}

void Emitter::setLife(float min, float max)
{
	this->minLife = min;
	this->maxLife = max;
}

void Emitter::setMaxLife(float max)
{
	this->maxLife = max;
}

void Emitter::setMinLife(float min)
{
	this->minLife = min;
}

float Emitter::getMaxLife() const
{
	return maxLife;
}

float Emitter::getMinLife() const
{
	return minLife;
}

void Emitter::setParticleVel(float min, float max)
{
	this->minInitParticleVel = min;
	this->maxInitParticleVel = max;
}

void Emitter::setMaxParticleVel(float max)
{
	this->maxInitParticleVel = max;
}

void Emitter::setMinParticleVel(float min)
{
	this->minInitParticleVel = min;
}

void Emitter::setParticleVelDir(const glm::vec3 dir)
{
	this->particleVelDir = dir;
}

void Emitter::setParticleVelConeAngle(float angle)
{
	this->particleVelConeAngle = angle;
}

float Emitter::getMaxParticleVel() const
{
	return maxInitParticleVel;
}

float Emitter::getMinParticleVel() const
{
	return minInitParticleVel;
}

glm::vec3 Emitter::getParticleVelDir() const
{
	return particleVelDir;
}

float Emitter::getParticleVelConeAngle() const
{
	return particleVelConeAngle;
}

void Emitter::setParticleAccel(float min, float max)
{
	this->minInitParticleAccel = min;
	this->maxInitParticleAccel = max;
}

void Emitter::setMaxParticleAccel(float max)
{
	this->maxInitParticleAccel = max;
}

void Emitter::setMinParticleAccel(float min)
{
	this->minInitParticleAccel = min;
}

void Emitter::setParticleAccelDir(const glm::vec3 dir)
{
	this->particleAccelDir = dir;
}

void Emitter::setParticleAccelConeAngle(float angle)
{
	this->particleAccelConeAngle = angle;
}

float Emitter::getMaxParticleAccel() const
{
	return maxInitParticleAccel;
}

float Emitter::getMinParticleAccel() const
{
	return minInitParticleAccel;
}

glm::vec3 Emitter::getParticleAccelDir() const
{
	return particleAccelDir;
}

float Emitter::getParticleAccelConeAngle() const
{
	return particleAccelConeAngle;
}

void Emitter::init(Mesh* mesh)
{
	int stride = 0;
	int offset = 0;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// mesh
	Triangle* triangles = mesh->getTriangles();
	int numTriangles = mesh->getNumTriangles();
	numVerts = numTriangles * 3;
	glGenBuffers(1, &meshBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, meshBuffer);
	glm::vec3* vertData = new glm::vec3[numVerts];
	for (int i = 0; i < numTriangles; i++)
	{
		vertData[i * 3] = triangles[i].a.position;
		vertData[i * 3 + 1] = triangles[i].b.position;
		vertData[i * 3 + 2] = triangles[i].c.position;
	}
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * numVerts, glm::value_ptr(vertData[0]), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, (void*)offset);

	// position
	glGenBuffers(1, &positionBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
	glBufferData(GL_ARRAY_BUFFER, numParticles * sizeof(Particle), NULL, GL_STREAM_DRAW);
	stride = sizeof(glm::vec3) * 3 + sizeof(float) * 2;
	offset = sizeof(float) * 2;
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride, (void*)offset);

	glVertexAttribDivisor(0, 0); 
	glVertexAttribDivisor(1, 1);

	// init particles
	for (int i = 0; i < numParticles; i++)
	{
		particles[i] = randomParticle();
	}


}

void Emitter::update(float deltaTime)
{
	velocity += acceleration * deltaTime;
	position += velocity * deltaTime;

	for (int i = 0; i < numParticles; i++)
	{
		particles[i].update(deltaTime);
		if (particles[i].dead())
			particles[i] = randomParticle();
	}

}

void Emitter::draw(MatrixStack & stack)
{
	// bind shader
	sf::Shader::bind(shader);
	glBindVertexArray(vao);
	// update uniforms
	glm::mat4 model = stack.resultMat() * glm::translate(position);
	sf::Glsl::Mat4 mat = sf::Glsl::Mat4(&model[0][0]);
	shader->setUniform("model", mat);
	// copy data over
	glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
	glBufferData(GL_ARRAY_BUFFER, numParticles * sizeof(Particle), NULL, GL_STREAM_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, numParticles * sizeof(Particle), &particles[0]);
	// draw
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glDrawArraysInstanced(GL_TRIANGLES, 0, numVerts, numParticles);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
}

Particle Emitter::randomParticle()
{
	float life = randomFloat() * (maxLife - minLife) + minLife;
	// position
	glm::vec3 pos = glm::vec3(randomFloat(), randomFloat(), randomFloat()) * spawnRadius;
	// velocity
	float speed = randomFloat() * (maxInitParticleVel - minInitParticleVel) + minInitParticleVel;
	glm::vec3 vel = randomVecInCone(particleVelDir, particleVelConeAngle) * speed;
	// acceleration
	float accel = randomFloat() * (maxInitParticleAccel - minInitParticleAccel) + minInitParticleAccel;
	glm::vec3 acc = randomVecInCone(particleAccelDir, particleAccelConeAngle) * accel;
	return Particle(life, pos, vel, acc);
}

float Emitter::randomFloat()
{
	return rand() / ((float)RAND_MAX);
}

glm::vec3 Emitter::randomVecInCone(glm::vec3 axis, float angle)
{
	float phi = randomFloat() * angle * 2 - angle;
	float theta = randomFloat() * angle * 2 - angle;
	glm::mat4 rot = glm::yawPitchRoll(phi, theta, 0.f);
	return rot * glm::vec4(axis, 1.f);
}
