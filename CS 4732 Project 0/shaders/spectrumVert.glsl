#version 430

uniform mat4 perspective;
uniform mat4 camera;
uniform mat4 model;
uniform float spectrum[512];
uniform bool isBox;
uniform float time;


in layout(location = 0) vec4 vPosition;
in layout(location = 1) vec3 normal;
in layout(location = 2) vec4 vColor;
in layout(location = 3) int index;

out vec4 fNormal;
out vec4 color;
out float spect;

out VS_OUT {
    vec3 normal;
} vs_out;

void main() 
{
	// Normal
	mat3 normalMatrix = transpose(inverse(mat3(camera * model)));
	fNormal = normalize(vec4(normalMatrix * normal, 0.f));
    vs_out.normal = fNormal.xyz;
	
	// Position
	mat4 endMat = perspective * camera * model;
	if(isBox)
		gl_Position = endMat * vPosition + vec4(0.f, 1.f, 0.f, 0.f) * spectrum[index] * 20.f;
	else if(!isBox)
		gl_Position = endMat * vPosition + endMat * fNormal * spectrum[index] * 0.1f;
	//gl_Position = gl_Position + vec4(0.f, sin(time * 8 + gl_Position.x), 0.f, 0.f);
	//gl_Position = endMat * (vPosition + fNormal * spectrum[index] * 0.4f);
	// Color
	color = vColor;
	
	spect = spectrum[index];
} 
