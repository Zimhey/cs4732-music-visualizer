#version 430

uniform mat4 perspective;
uniform mat4 camera;
uniform mat4 model;

in layout(location = 0) vec4 vPosition;
in layout(location = 1) vec4 normal;

out vec4 fNormal;

void main() 
{
	mat4 endMat = perspective * camera * model;
	gl_Position = endMat * vPosition;
	fNormal = normal;
} 
