#version 430

layout (triangles) in;
layout (line_strip, max_vertices = 2) out;

in VS_OUT {
    vec3 normal;
} gs_in[];

const float MAGNITUDE = 0.5f;

void GenerateLine(int index)
{
    gl_Position = gl_in[index].gl_Position;
    EmitVertex();
    gl_Position = gl_in[index].gl_Position + vec4(gs_in[index].normal, 0.0) * MAGNITUDE;
    EmitVertex();
    EndPrimitive();
}

vec3 GetNormal()
{
   vec3 a = vec3(gl_in[0].gl_Position) - vec3(gl_in[1].gl_Position);
   vec3 b = vec3(gl_in[2].gl_Position) - vec3(gl_in[1].gl_Position);
   return normalize(cross(a, b));
}  

void FaceNormal(vec3 norm)
{
	// Find center
	gl_Position = (gl_in[0].gl_Position + gl_in[1].gl_Position + gl_in[2].gl_Position) / 3.f;
	EmitVertex();
	gl_Position = gl_Position + vec4(norm, 0.f) * MAGNITUDE;
	EmitVertex();
}

void main() 
{
    //GenerateLine(0); // first vertex normal
    //GenerateLine(1); // second vertex normal
    //GenerateLine(2); // third vertex normal
	FaceNormal(gs_in[0].normal);
	//FaceNormal(GetNormal());
} 
