/* Module     : Application.cpp
* Author      : Corey Dixon
* Email       : cdixon@wpi.edu
* Course      : CS4732
*
* Description : The implementation of the Application class. This class handles
* loading a basic shader, initializing the scene, drawing the scene and
* updating the scene.
*
* Date        : 2017/03/16
*
* (c) Copyright 2017, Corey Dixon.
*/
#include "Application.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <SFML\Graphics\Glsl.hpp>
#include <iostream>

using namespace glm;

/* ----------------------------------------------------------------------- */
/* Function    : Application(sf::Window* window)
*
* Description : Constructs the Application
*
* Parameters  : SF::Window* window : A SFML window with an OpenGL context
*/
Application::Application(sf::Window* window)
{
	this->window = window;
	this->soundManager = new SoundManager();
	loopTime.restart();
	totalTime = 0.f;
}

/* ----------------------------------------------------------------------- */
/* Function    : ~Application()
*
* Description : Destructor
*/
Application::~Application()
{
}

/* ----------------------------------------------------------------------- */
/* Function    : void initShaders()
*
* Description : Initializes the basic shader
*/
void Application::initShaders()
{
	if (!basicShader.loadFromFile("shaders/vertex.glsl", "shaders/fragment.glsl"))
		std::cout << "Failed to load basic shader" << std::endl;
	if (!spectrumShader.loadFromFile("shaders/spectrumVert.glsl", "shaders/spectrumFrag.glsl"))
		std::cout << "Failed to load spectrum shader" << std::endl;
	if (!normalsShader.loadFromFile("shaders/spectrumVert.glsl","shaders/normalsGeo.glsl", "shaders/fragment.glsl"))
		std::cout << "Failed to load normals shader" << std::endl;

	glClearColor(0.f, 0.f, 0.f, 1.f);
}

/* ----------------------------------------------------------------------- */
/* Function    : void initCamera()
*
* Description : Initializes the camera
*/
void Application::initCamera()
{
	cameraPos = { 0.f, 0.f, 5.f };
	cameraUp = vecUp;
	cameraFront = { 0.f, 0.f, -1.f }; 
}

/* ----------------------------------------------------------------------- */
/* Function    : void initScene()
*
* Description : Initializes a cube in the scene
*/
void Application::initScene()
{
	srand(time(NULL));

	window->setMouseCursorGrabbed(true);
	window->setMouseCursorVisible(false);

	// plane
	//spectrumPlane = new Plane(NUM_BUCKETS - 3);
	spectrumPlane = new Plane(soundManager->NUM_BUCKETS - 3);
	//spectrumShader.setUniform("isBox", false);
	spectrumPlane->setShader(spectrumShader);
	spectrumPlane->setPosition(glm::vec3(0.f, 1.f, 0.f));
	spectrumPlane->setScale(glm::vec3(50.f, 1.f, 3.f));
	spectrumPlane->setWireFrame(true);
	
	model = new PlyModel("ply/cow.ply", soundManager->NUM_BUCKETS / 2);
	model->setShader(spectrumShader);
	model->setPosition(glm::vec3(0.f, 1.f, 10.f));
	model->setScale(glm::vec3(25.f, 25.f, 25.f));
	model->setWireFrame(true);

	models = new PlyModel*[3];
	
	models[0] = new PlyModel("ply/stratocaster.ply", soundManager->NUM_BUCKETS / 2);
	models[0]->setShader(spectrumShader);
	models[0]->setPosition(glm::vec3(25.f, 0.f, 20.f));
	models[0]->setScale(glm::vec3(25.f, 25.f, 25.f));
	models[0]->setWireFrame(true);

	models[1] = new PlyModel("ply/tommygun.ply", soundManager->NUM_BUCKETS / 2);
	models[1]->setShader(spectrumShader);
	models[1]->setPosition(glm::vec3(-25.f, 30.f, -10.f));
	models[1]->setScale(glm::vec3(15.f, 15.f, 15.f));
	models[1]->setWireFrame(true);

	models[2] = new PlyModel("ply/teapot.ply", soundManager->NUM_BUCKETS / 2);
	models[2]->setShader(spectrumShader);
	models[2]->setPosition(glm::vec3(20.f, 30.f, 10.f));
	models[2]->setScale(glm::vec3(20.f, 20.f, 20.f));
	models[2]->setWireFrame(true);


	//sphere
	//spectrumSphere = new SpectrumSphere(NUM_BUCKETS - 3);
	spectrumSphere = new SpectrumSphere(soundManager->NUM_BUCKETS - 3);
	spectrumSphere->setShader(spectrumShader);
	spectrumSphere->setPosition(glm::vec3(5.f, 8.5f, -20.f));
	spectrumSphere->setScale(glm::vec3(1.5f, 1.5f, 1.5f));
	spectrumSphere->setWireFrame(true);

	aSphere = new Sphere(64);
	aSphere->setSpectrumToModel(soundManager->NUM_BUCKETS / 3);
	aSphere->setShader(spectrumShader);
	aSphere->setPosition(glm::vec3(5.f, 8.5f, -20.f));
	aSphere->setScale(glm::vec3(5.5f, 5.5f, 5.5f));
	aSphere->setWireFrame(true);

	// box
	//spectrumBox = new SpectrumBox(NUM_BUCKETS - 3);
	spectrumBox = new SpectrumBox(soundManager->NUM_BUCKETS - 3);
	spectrumBox->setShader(spectrumShader);
	spectrumBox->setPosition(glm::vec3(0.f, -5.f, -10.f));
	spectrumBox->setScale(glm::vec3(1 / 2.f, 1 / 2.f, 5.f));
	spectrumBox->setWireFrame(true);


	//plane = new Plane(NUM_BUCKETS);
	plane = new Plane(soundManager->NUM_BUCKETS);
	plane->setShader(spectrumShader);
	plane->setPosition(glm::vec3(0.f, 1.f, 0.f));
	plane->setScale(glm::vec3(60.f, 1.f, 1.f));
	plane->setWireFrame(true);

	pause = false;

	// song
	const char* songName = "music/BusMoney.mp3";
	//spectrumArray = new float[NUM_BUCKETS];
	spectrumArray = new float[soundManager->NUM_BUCKETS];
	/*
	// fmod init
	FMOD::System_Create(&fmodSys);
	fmodSys->init(2, FMOD_INIT_NORMAL, NULL);
	fmodSys->getMasterChannelGroup(&channelGroup);

	// http://www.pixelnerve.com/v/2017/03/16/get-fft-on-fmod-5/
	fmodSys->createDSPByType(FMOD_DSP_TYPE_FFT, &fft);
	fft->setParameterInt(FMOD_DSP_FFT_WINDOWTYPE, FMOD_DSP_FFT_WINDOW_RECT);
	//fft->setParameterInt(FMOD_DSP_FFT_WINDOWTYPE, FMOD_DSP_FFT_WINDOW_TRIANGLE);
	// FMOD_DSP_FFT_WINDOW_HANNING
	fft->setParameterInt(FMOD_DSP_FFT_WINDOWSIZE, SPECTRUM_SIZE * 2);
	channelGroup->addDSP(FMOD_CHANNELCONTROL_DSP_HEAD, fft);

	fmodSys->createStream(songName, FMOD_DEFAULT, NULL, &music);
	fmodSys->playSound(music, NULL, false, &channel);
	*/
	soundManager->Play();

}
/* ----------------------------------------------------------------------- */
/* Function    : void drawScene()
*
* Description : Clears the screen, updates the matricies in the shader
* and draws all the objects in the scene
*/
void Application::drawScene()
{
	//glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	// clear both the color and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	GLfloat aspectRatio = ((GLfloat)window->getSize().x) / window->getSize().y;
	perspective = glm::perspective(45.f, aspectRatio, 0.1f, 200.f);

	sf::Glsl::Mat4 mat(&perspective[0][0]); // TODO shader management
	basicShader.setUniform("perspective", mat);
	spectrumShader.setUniform("perspective", mat);
	spectrumShader.setUniform("time", totalTime);
	normalsShader.setUniform("perspective", mat);


	//camera = glm::lookAt(cameraPos, { 0.f, 0.f, 0.f } , { 0.f, 1.f, 0.f });
	camera = glm::lookAt(cameraPos, cameraPos + cameraFront, vecUp);
	mat = sf::Glsl::Mat4(&camera[0][0]);
	basicShader.setUniform("camera", mat);
	spectrumShader.setUniform("camera", mat);
	normalsShader.setUniform("camera", mat);

	MatrixStack stack;
	//spectrumPlane->draw(stack);
	plane->setPosition(glm::vec3(0.f, -2.f, 0.f));
	//plane->draw(stack);
	plane->setPosition(glm::vec3(0.f, 0.f, 0.f));
	//plane->draw(stack);
	spectrumShader.setUniform("isBox", true);
	spectrumBox->draw(stack);

	spectrumShader.setUniform("isBox", false);
	normalsShader.setUniform("isBox", false);
	//spectrumSphere->draw(stack);

	// A sphere
	aSphere->setShader(spectrumShader);
	aSphere->draw(stack);
	aSphere->setShader(normalsShader);
	//aSphere->draw(stack);

	// cow
	model->setShader(spectrumShader);
	model->draw(stack);
	model->setShader(normalsShader);
	//model->draw(stack);

	// misc ply files
	for (int i = 0; i < 3; i++)
	{
		models[i]->setShader(spectrumShader);
		models[i]->draw(stack);
		models[i]->setShader(normalsShader);
		//models[i]->draw(stack);
	}



}

void Application::calcSpectrum()
{
	void* spectrumData;
	//fmodSys->update();
	soundManager->fmodSys->update();
	//fft->getParameterData(FMOD_DSP_FFT_SPECTRUMDATA, (void**)&spectrumData, 0, 0, 0);
	soundManager->fft->getParameterData(FMOD_DSP_FFT_SPECTRUMDATA, (void**)&spectrumData, 0, 0, 0);
	FMOD_DSP_PARAMETER_FFT* data = (FMOD_DSP_PARAMETER_FFT*) spectrumData;

	if (data != NULL)
	{
		if (data->numchannels > 0)
		{
			//float freqStart = START_FREQ;
			float freqStart = soundManager->START_FREQ;
			int binStart = 0;

			for (int i = 0; i < soundManager->NUM_BUCKETS; i++) // for each bucket
			{
				spectrumArray[i] = 0.f;
				//spectrumArray[i] = -INFINITY;
				float freqEnd = freqStart * soundManager->scale;
				float difference = freqEnd - freqStart;
				int numBins = difference / soundManager->BIN_FREQ_RANGE;
				binStart = freqStart / soundManager->BIN_FREQ_RANGE;
				int binEnd = binStart + numBins;

				if (binEnd > soundManager->SPECTRUM_SIZE - 1)
					binEnd = soundManager->SPECTRUM_SIZE - 1;
				
				
				for (int j = binStart; j < binEnd; j++) // sum all bins in freq range of bar
				{
					spectrumArray[i] += data->spectrum[0][j];
					//if (data->spectrum[0][j] > spectrumArray[i])
					//	spectrumArray[i] = data->spectrum[0][j];
				}
				//spectrumArray[i] /= numBins;
				// good
				spectrumArray[i] += data->spectrum[0][(binStart + binEnd) / 2] * 20 * log10((freqStart + freqEnd) / 2.f) / 10.f;
				//spectrumArray[i] *= 20 * log10((freqStart + freqEnd) / 2.f);


				//spectrumArray[i] /= numBins; // average
				//spectrumArray[i] = abs(-20 * log10(data->spectrum[0][(binStart + binEnd) / 2]) / 40.f);
				
			//spectrumArray[i] += data->spectrum[0][(binStart + binEnd) / 2] * ((freqStart + freqEnd)) / 8;
				//spectrumArray[i] += data->spectrum[0][(binStart + binEnd) / 2];
			//	spectrumArray[i] = log10(spectrumArray[i]);
				//cout << spectrumArray[i] << endl;
				// end
				freqStart = freqEnd;
			}
		}
		spectrumShader.setUniformArray("spectrum", spectrumArray, soundManager->NUM_BUCKETS);
		normalsShader.setUniformArray("spectrum", spectrumArray, soundManager->NUM_BUCKETS);
	}
}

/* ----------------------------------------------------------------------- */
/* Function    : void updateScene()
*
* Description : Updates the scene
*
*/
void Application::updateScene()
{
	float deltaTime = loopTime.getElapsedTime().asSeconds();
	totalTime += deltaTime;
	//Update where the camera is looking
	cameraDir = normalize(cameraPos - cameraTarget);
	cameraRight = normalize(cross(cameraUp, cameraDir));

	// camera speed and rotational pitch control
	float cameraMoveSpeed = 25 / 2.f * deltaTime;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
		cameraMoveSpeed *= 3.f;

	//Move Forward
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		cameraPos += cameraMoveSpeed * cameraFront;
	}

	//Move Backwards
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		cameraPos -= cameraMoveSpeed * cameraFront;
	}

	//Strafe Left
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		cameraPos -= cameraMoveSpeed * normalize(cross(cameraFront, cameraUp));
	}

	//Strafe Right
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		cameraPos += cameraMoveSpeed * normalize(cross(cameraFront, cameraUp));
	}
	
	
	//Game loop
	// Process events
	sf::Event event;
	while (window->pollEvent(event))
	{
		// Close window: exit
		if (event.type == sf::Event::Closed)
			window->close();

		// Escape key: exit
		if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Escape))
			window->close();

		//Toggle the pause of the song
		if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::P))
		{
			pause = !pause;
			//channel->setPaused(pause);
			soundManager->TogglePause(pause);
		}

		//Restart the song 
		if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::R))
		{
			//channel->setPosition(0, FMOD_TIMEUNIT_PCM);
			soundManager->RestartSong();
		}

		//Play the next song
		if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::M))
		{
			//channel->setPosition(0, FMOD_TIMEUNIT_PCM);
			soundManager->PlayNext();
		}
		//Play the previous song
		if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::N))
		{
			//channel->setPosition(0, FMOD_TIMEUNIT_PCM);
			soundManager->PlayPrev();
		}

		if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Num1))
		{
			spectrumShader.setUniform("outputType", 0);
		}

		if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Num2))
		{
			spectrumShader.setUniform("outputType", 1);
		}

		if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Num3))
		{
			spectrumShader.setUniform("outputType", 2);
		}

		if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Num4))
		{
			spectrumShader.setUniform("outputType", 3);
		}
		if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Num5))
		{
			spectrumShader.setUniform("outputType", 4);
		}

		if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::F))
		{
			model->setWireFrame(false);
			spectrumBox->setWireFrame(false);
			spectrumPlane->setWireFrame(false);
			for (int i = 0; i < 3; i++)
				models[i]->setWireFrame(false);
		}

		if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::G))
		{
			model->setWireFrame(true);
			spectrumBox->setWireFrame(true);
			spectrumPlane->setWireFrame(true);
			for (int i = 0; i < 3; i++)
				models[i]->setWireFrame(true);
		}


		// Resize event: adjust the viewport
		if (event.type == sf::Event::Resized)
			glViewport(0, 0, event.size.width, event.size.height);
		
		//Mouse Movement
		if (event.type == sf::Event::MouseMoved)
		{
			//Will set the initial mouse value
			if (firstMouse)
			{
				lastX = event.mouseMove.x;
				lastY = event.mouseMove.y;
				firstMouse = false;
			}

			sf::Vector2i windowCenter = sf::Vector2i(window->getSize().x / 2, window->getSize().y / 2);

			//Mouse movement
			xOffset = windowCenter.x - event.mouseMove.x;
			yOffset = windowCenter.y - event.mouseMove.y;;
			//Make sure the mouse does not move and hide mouse cursor

			//sf::Mouse::setPosition(windowCenter, *window);
			if (xOffset != 0 || yOffset != 0)
				sf::Mouse::setPosition(windowCenter, *window);
			
			//Mouse sensitivity
			//Rotate the camera based on the mouse movement
			yaw += -xOffset * sensitivity;
			pitch += yOffset * sensitivity;
			//Make sure you can't break the y-axis 
			if (pitch > 89.f)
				pitch = 89.f;
			else if (pitch < -89.f)
				pitch = -89.f;
			//Set the new front of the camera
			newFront.x = cos(radians(yaw)) * cos(radians(pitch));
			newFront.y = sin(radians(pitch));
			newFront.z = sin(radians(yaw)) * cos(radians(pitch));
			cameraFront = normalize(newFront);
			
		}
		
	}
	
	loopTime.restart();
	calcSpectrum();

	

	

	
	/*
	// move camera up
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		cameraPos.y += cameraMoveSpeed * deltaTime;
	}
	// move camera down
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
	{
		cameraPos.y -= cameraMoveSpeed * deltaTime;
	}
	*/
}

glm::vec3 Application::randomPosition() const
{
	vec3 randVec = vec3(rand() / ((float)RAND_MAX), rand() / ((float)RAND_MAX), rand() / ((float)RAND_MAX));
	return randVec; // TODO check this
}
