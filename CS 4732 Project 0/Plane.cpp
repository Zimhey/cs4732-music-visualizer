/* Module     : Plane.cpp
* Author      : Corey Dixon
* Email       : cdixon@wpi.edu
* Course      : CS4732
*
* Description : Implementation of the Plane class. This class is a
* child of Mesh and generates the vertex data for a Plane
*
* Date        : 2017/04/07
*
* (c) Copyright 2017, Corey Dixon.
*/

#include "Plane.h"



Plane::Plane() : Mesh()
{
	Vertex verts[4];
	float bounds = 0.5f; // create a plane from -0.5 to 0.5 (i.e. size 1)

	// raw vert data
	verts[0].position = { -1 * bounds, 0, -1 * bounds }; // sw
	verts[1].position = { 1 * bounds, 0, -1 * bounds }; // se
	verts[2].position = { 1 * bounds, 0, 1 * bounds }; // ne
	verts[3].position = { -1 * bounds, 0, 1 * bounds }; // nw

	Quad quad;
	quad.a = verts[0];
	quad.b = verts[1];
	quad.c = verts[2];
	quad.d = verts[3];
	addQuad(quad);

	usingSpectrum = true;
	usingColorAttrib = true;

	setGradientOnX(glm::vec3(1.f, 1.f, 0.f), glm::vec3(1.f, 0.f, 1.f));

	genFaceNormals();
	sendToGPU();
}

Plane::Plane(int segments) : Mesh()
{
	Vertex **verts;
	float bounds = 0.5f; // create a plane from -0.5 to 0.5 (i.e. size 1)
	usingSpectrum = true;
	usingColorAttrib = true;

	verts = new Vertex*[segments];
	for (int i = 0; i < segments; i++) // x
	{
		verts[i] = new Vertex[segments];
		float x = (i * 1.f / segments) * 2 * bounds - bounds;
		for (int j = 0; j < segments; j++) // z
		{
			float z = (j * 1.f / segments) * 2 * bounds - bounds;
			verts[i][j].position.x = x;
			verts[i][j].position.y = 0.f;
			verts[i][j].position.z = z;
			verts[i][j].color = position;
			verts[i][j].index = i;
		}
	}

	Quad quad;
	for (int i = 0; i < segments - 1; i++)
	{
		for (int j = 0; j < segments - 1; j++)
		{
			quad.a = verts[i][j];
			quad.b = verts[i + 1][j];
			quad.c = verts[i + 1][j + 1];
			quad.d = verts[i][j + 1];
			addQuad(quad);
		}
	}

	usingSpectrum = true;
	usingColorAttrib = true;

	setGradientOnX(glm::vec3(1.f, 1.f, 0.f), glm::vec3(1.f, 0.f, 1.f));

	genFaceNormals();
	sendToGPU();

	for (int i = 0; i < segments; i++)
		delete[] verts[i];
	delete[] verts;
}


Plane::~Plane()
{
}
