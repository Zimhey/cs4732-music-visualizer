/* Module     : Sphere.cpp
* Author      : Corey Dixon
* Email       : cdixon@wpi.edu
* Course      : CS4732
*
* Description : Implementation of the Sphere class. This class is a
* child of Mesh and generates the vertex data for a Cube
*
* Date        : 2017/03/20
*
* (c) Copyright 2017, Corey Dixon.
*/
#include "Sphere.h"

/**
* Constructs a Sphere of size 1
*/
Sphere::Sphere() : Sphere(64)
{

}

Sphere::Sphere(int numSegments) : Mesh()
{
	Vertex a, b, c;
	// bottom from stack 0 to 1
	a = genVert(0, 0, numSegments);
	b = genVert(1, 0, numSegments);
	for (int i = 1; i < numSegments; i++)
	{
		c = genVert(1, i, numSegments);
		addTriangle(a, b, c);
		b = c;
	}
	c = genVert(1, 0, numSegments);
	addTriangle(a, b, c);

	// mid from stack 1 to n - 1
	Quad quad;
	for (int stack = 1; stack < numSegments - 1; stack++)
	{
		for (int slice = 0; slice < numSegments - 1; slice++)
		{
			quad.a = genVert(stack + 1, slice, numSegments); // top left
			quad.d = genVert(stack + 1, slice + 1, numSegments); // top right
			quad.c = genVert(stack, slice + 1, numSegments); // bottom right
			quad.b = genVert(stack, slice, numSegments); // bottom left
			addQuad(quad);
			quad.a = quad.b;
			quad.d = quad.c;
		}
		quad.b = genVert(stack + 1, 0, numSegments);
		quad.c = genVert(stack, 0, numSegments);
		addQuad(quad);
	}

	// top from stack n - 1 to n
	a = genVert(numSegments, 0, numSegments);
	b = genVert(numSegments - 1, 0, numSegments);
	for (int i = 1; i < numSegments; i++)
	{
		c = genVert(numSegments - 1, i, numSegments);
		addTriangle(c, b, a);
		b = c;
	}
	c = genVert(numSegments - 1, 0, numSegments);
	addTriangle(c, b, a);

	genFaceNormals();
	sendToGPU();
}

/**
* Destructor
*/
Sphere::~Sphere()
{
}

/**
* Generate a Vertex for the sphere
* @param stack the height segment
* @param slice the segment around the circle we are at
* @param segments the total number of segments
*/
Vertex Sphere::genVert(int stack, int slice, int segments)
{
	// https://www.opengl.org/discussion_boards/showthread.php/159584-sphere-generation
	Vertex vert;
	float bounds = 0.5f;
	vert.position.y = -cos(3.14 * stack / segments);
	float r = sqrt(1 - pow(vert.position.y, 2));
	vert.position.y = vert.position.y * bounds;
	vert.position.x = r * sin(2 * 3.14 * slice / segments) * bounds;
	vert.position.z = r * cos(2 * 3.14 * slice / segments) * bounds;

	return vert;
}
