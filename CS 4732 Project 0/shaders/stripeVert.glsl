#version 430

uniform mat4 perspective;
uniform mat4 camera;
uniform mat4 model;

in layout(location = 0) vec4 vPosition;
in layout(location = 1) vec4 normal;
//in layout(location = 2) vec4 vColor;
in layout(location = 3) vec2 texCoord;

uniform vec4 blue = vec4(0.0, 0.0, 1.0, 1.0);
uniform vec4 red = vec4(1.0, 0.0, 0.0, 0.5);
out vec2 texCoordV;

void main()
{
	texCoordV = texCoord;
	gl_Position = 0;
}


