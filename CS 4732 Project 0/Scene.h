#pragma once
class Scene
{
public:
	Scene();
	~Scene();
	virtual void draw() = 0;
	virtual void update() = 0;

private:

};

