Project3: Boids of A Feather!
Authors: Corey Dixon, Marco Duran
Emails:  cdixon@wpi.edu, mdduran@wpi.edu

We have made an aquarium that has fish and a treasure chest obstacle that shoots bubbles. 

Open the solution file in visual studio
Build the application for x86 (x64 will not run due to libraries being x86)
Run the application
The animation should start immediately
Push space bar to raise the camera
Push left shift to lower the camera
Push esc to close the program

Using SFML for window and OpenGL context generation
Also using SFML for some Shader abstraction
Using glm for its matrix and vector classes

The libraries should not need to be installed as they are
included in the project. 

YouTube Link: https://www.youtube.com/watch?v=xuF0D3semAc