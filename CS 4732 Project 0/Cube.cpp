/* Module     : Cube.cpp
* Author      : Corey Dixon
* Email       : cdixon@wpi.edu
* Course      : CS4732
*
* Description : Implementation of the Cube class. This class is a
* child of Mesh and generates the vertex data for a Cube
*
* Date        : 2017/03/20
*
* (c) Copyright 2017, Corey Dixon.
*/
#include "Cube.h"

/**
* Constructs a Cube of size 1
*/
Cube::Cube() : Mesh()
{
	Vertex verts[8];
	float bounds = 0.5f; // create a cube from -0.5 to 0.5 (i.e. size 1)

	// raw vert data
	verts[0].position = { -bounds, -bounds, -bounds };
	verts[1].position = { bounds, -bounds, -bounds };
	verts[2].position = { bounds, -bounds, bounds };
	verts[3].position = { -bounds, -bounds, bounds };

	verts[4].position = { -bounds, bounds, -bounds };
	verts[5].position = { bounds, bounds, -bounds };
	verts[6].position = { bounds, bounds, bounds };
	verts[7].position = { -bounds, bounds, bounds };

	// raw index data
	int cubeIndices[] = {
		0,3,2,1,
		2,3,7,6,
		0,4,7,3,
		1,2,6,5,
		4,5,6,7,
		0,1,5,4
	};

	Quad quad;
	for (int i = 0; i < 6; i++) // generate quads for each side
	{
		quad.a = verts[cubeIndices[i * 4 + 0]];
		quad.b = verts[cubeIndices[i * 4 + 1]];
		quad.c = verts[cubeIndices[i * 4 + 2]];
		quad.d = verts[cubeIndices[i * 4 + 3]];
		addQuad(quad);
	}
	
	genFaceNormals(); // generate face normals
	sendToGPU(); // update GPU
}

/**
* Destructor
*/
Cube::~Cube()
{
}
