#pragma once
#include "Scene.h"
#include "Application.h"
class TimeShiftPlane :
	public Mesh
{
public:
	TimeShiftPlane(FMOD::DSP &spectrum);
	TimeShiftPlane(FMOD::DSP &spectrum, int segments);
	~TimeShiftPlane();
};

