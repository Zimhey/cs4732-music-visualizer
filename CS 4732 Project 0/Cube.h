/* Module     : Cube.h
* Author      : Corey Dixon
* Email       : cdixon@wpi.edu
* Course      : CS4732
*
* Description : Header file for the Cube class. This class is a
* child of Mesh and generates the vertex data for a Cube
*
* Date        : 2017/03/20
*
* (c) Copyright 2017, Corey Dixon.
*/
#pragma once
#include "Mesh.h"
class Cube :
	public Mesh
{
public:
	Cube();
	~Cube();
};

