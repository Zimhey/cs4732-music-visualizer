#pragma once
#include <fmod.hpp>
#include <glm/matrix.hpp>
using namespace FMOD;

class SoundManager
{
public:
	SoundManager();
	~SoundManager();
	void Play();
	void PlayNext();
	void TogglePause(bool isPaused);
	void PlayPrev();
	void RestartSong();
	void StopSong();
	Sound** GetSongList();
	//FMOD INIT
	System *fmodSys;
	Channel *channel;
	ChannelGroup *channelGroup;
	DSP *fft;
	
	int NUMSONGS = 6;
	const int SPECTRUM_SIZE = 512;
	const int NUM_BUCKETS = 256;
	const float NYQUIST_FREQ = 22050.f;
	const float START_FREQ = 20.f;

	const float BIN_FREQ_RANGE = NYQUIST_FREQ / (SPECTRUM_SIZE * 2);
	// http://www.wolframalpha.com/input/?i=20*(1+%2B+x)+%5Ey%3D+22000+solve+x
	const float scale = pow((NYQUIST_FREQ / START_FREQ), 1.f / NUM_BUCKETS); // x axis generator

	float* spectrumArray;

private:
	

	
	Sound** listOfSongs;
	char** listOfSongNames;
	int playlistIndex;
};

