#pragma once
#include <glm/vec3.hpp>
using namespace glm;
class ColorVec
{
public:
	static const vec3 red;
	static const vec3 green;
	static const vec3 blue;
	ColorVec();
	~ColorVec();
};

