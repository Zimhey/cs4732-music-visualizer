#include "SpectrumSphere.h"



SpectrumSphere::SpectrumSphere(): SpectrumSphere(64)
{
}

SpectrumSphere::SpectrumSphere(int numBuckets) : Mesh()
{
	Vertex a, b, c;
	// bottom from stack 0 to 1
	a = genVert(0, 0, numBuckets);
	b = genVert(1, 0, numBuckets);
	for (int i = 1; i < numBuckets; i++)
	{
		c = genVert(1, i, numBuckets);
		addTriangle(a, b, c);
		b = c;
	}
	c = genVert(1, 0, numBuckets);
	addTriangle(a, b, c);

	// mid from stack 1 to n - 1
	Quad quad;
	for (int stack = 1; stack < numBuckets - 1; stack++)
	{

		for (int slice = 0; slice < numBuckets - 1; slice++)
		{
			quad.a = genVert(stack + 1, slice, numBuckets); // top left
			quad.a.index =  cos(slice) * slice++;
			quad.b = genVert(stack + 1, slice + 1, numBuckets); // top right
			quad.b.index = sin(slice) * slice;
			quad.c = genVert(stack, slice + 1, numBuckets); // bottom right
			quad.c.index = sin(slice) * slice;
			quad.d = genVert(stack, slice, numBuckets); // bottom left
			quad.d.index = cos(slice) * slice++;
			addQuad(quad);
			quad.a = quad.b;
			quad.d = quad.c;
		}
		quad.b = genVert(stack + 1, 0, numBuckets);
		quad.b.index = stack++;
		quad.c = genVert(stack, 0, numBuckets);
		quad.c.index = stack++;
		addQuad(quad);
	}

	// top from stack n - 1 to n
	a = genVert(numBuckets, 0, numBuckets);
	b = genVert(numBuckets - 1, 0, numBuckets);
	for (int i = 1; i < numBuckets; i++)
	{
		c = genVert(numBuckets - 1, i, numBuckets);
		addTriangle(c, b, a);
		b = c;
	}
	c = genVert(numBuckets - 1, 0, numBuckets);
	addTriangle(c, b, a);

	usingSpectrum = true;
	usingColorAttrib = true;

	setGradientOnX(glm::vec3(1.f, 1.f, 0.f), glm::vec3(1.f, 0.f, 1.f));

	genFaceNormals();
	sendToGPU();
}


SpectrumSphere::~SpectrumSphere()
{
}

/**
* Generate a Vertex for the sphere
* @param stack the height segment
* @param slice the segment around the circle we are at
* @param segments the total number of segments
*/
Vertex SpectrumSphere::genVert(int stack, int slice, int segments)
{
	// https://www.opengl.org/discussion_boards/showthread.php/159584-sphere-generation
	Vertex vert;
	float bounds = 0.5f;
	vert.position.y = -cos(3.14 * stack / segments);
	float r = sqrt(1 - pow(vert.position.y, 2));
	vert.position.y = vert.position.y * bounds;
	vert.position.x = r * sin(2 * 3.14 * slice / segments) * bounds;
	vert.position.z = r * cos(2 * 3.14 * slice / segments) * bounds;

	return vert;
}
