#include "Particle.h"



Particle::Particle() : Particle(0.f)
{
}

Particle::Particle(float lifeTime) : Particle(lifeTime, glm::vec3())
{
}

Particle::Particle(float lifeTime, const glm::vec3 position) : Particle(lifeTime, position, glm::vec3())
{
}

Particle::Particle(float lifeTime, const glm::vec3 position, const glm::vec3 velocity) : Particle(lifeTime, position, velocity, glm::vec3())
{
}

Particle::Particle(float lifeTime, const glm::vec3 position, const glm::vec3 velocity, const glm::vec3 acceleration)
{
	this->lifeTime = lifeTime;
	this->age = 0.f;
	this->position = position;
	this->velocity = velocity;
	this->acceleration = acceleration;
}


Particle::~Particle()
{
}

float Particle::getLifeTime() const
{
	return lifeTime;
}

void Particle::setLifeTime(float lifeTime)
{
	this->lifeTime = lifeTime;
}

float Particle::getAge() const
{
	return age;
}

void Particle::setAge(float age)
{
	this->age = age;
}

glm::vec3 Particle::getPosition() const
{
	return position;
}

void Particle::setPosition(const glm::vec3 position)
{
	this->position = position;
}

glm::vec3 Particle::getVelocity() const
{
	return velocity;
}

void Particle::setVelocity(const glm::vec3 velocity)
{
	this->velocity = velocity;
}

glm::vec3 Particle::getAcceleration() const
{
	return acceleration;
}

void Particle::setAcceleration(const glm::vec3 acceleration)
{
	this->acceleration;
}

void Particle::update(const float deltaTime)
{
	age += deltaTime;
	velocity += acceleration * deltaTime;
	position += velocity * deltaTime;
}

bool Particle::dead() const
{
	return age >= lifeTime;
}
