#pragma once
#include "Mesh.h"
class SpectrumSphere :
	public Mesh
{
public:
	SpectrumSphere();
	SpectrumSphere(int numBuckets);
	~SpectrumSphere();

private:
	Vertex genVert(int stack, int slice, int segments);
};

