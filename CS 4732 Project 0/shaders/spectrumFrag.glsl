#version 430


in vec4 fNormal;
in vec4 color;
in float spect;

out vec4 fragColor;

uniform int outputType;
uniform float time;

//https://stackoverflow.com/questions/15095909/from-rgb-to-hsv-in-opengl-glsl
vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));
    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

//https://stackoverflow.com/questions/15095909/from-rgb-to-hsv-in-opengl-glsl
vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec4 normalColor()
{
	vec4 c = fNormal * 0.5f + 0.5f;
	c.a = 1;
	return c;
}

void main() 
{ 
	switch(outputType)
	{
		case 0:
			fragColor = color;
			break;
			
		case 1:
			//fragColor = gl_FragCoord;
			vec3 hsv = vec3(spect, 1.f, 1.f);
			fragColor = vec4(hsv2rgb(hsv), 1.f) * 0.7f + normalColor() *0.3f;
			break;
			
		case 2:
			fragColor = normalColor();
			break;
			
		case 3:
			float zNear = 0.1f;
			float zFar = 200.f;
			vec3 c = vec3(0.f, 1.f, 1.f) * (gl_FragCoord.z / (zFar - zNear));  ;// * (2.0 * zNear) / (zFar + zNear - gl_FragCoord.z * (zFar - zNear));
			c += vec3(1.f, 0.f, 0.f);
			fragColor = vec4(c, 1.f);
			break;
			
		case 4:
			fragColor = vec4(hsv2rgb(vec3(sin(time) * 0.5f + 0.5f, 1.f, 1.f)), 1.f);
			fragColor = normalColor() * fragColor;
			break;
			
		default:
			fragColor = vec4(1.f, 1.f, 1.f, 1.f);
	}
} 

