/* Module     : Mesh.h
* Author      : Corey Dixon
* Email       : cdixon@wpi.edu
* Course      : CS4732
*
* Description : Header file for the Mesh class. This class
* abstracts much of the OpenGL. The mesh can be given a 
* position, rotation and scale. It provides functions for
* adding additional Triangles and Quads. It also has a
* generic draw function which updates the model matrices
* in the shader.
*
* Date        : 2017/03/20
*
* (c) Copyright 2017, Corey Dixon.
*/
#pragma once
#include <GL/glew.h>
#include <SFML\Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <glm\vec3.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <iostream>
#include "MatrixStack.h"

struct Vertex
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec3 color;
	int index;
};

struct Triangle
{
	Vertex a, b, c;
};

struct Quad
{
	Vertex a, b, c, d;
};

class Mesh
{
public:
	Mesh();
	~Mesh();

	virtual void draw(); // TODO find new function names
	virtual void draw(MatrixStack &stack);


	void setPosition(glm::vec3 position);
	glm::vec3 getPosition() const;
	void setScale(glm::vec3 scale);
	glm::vec3 getScale() const;
	void setRotation(glm::mat4 rotation);
	glm::mat4 getRotation() const;
	glm::mat4 getModelViewMatrix() const;
	void setShader(sf::Shader& shader);
	void setWireFrame(bool useWireframe);
	//TODO change color
	void setGradientOnX(glm::vec3 start, glm::vec3 end); // could generalize to any axis
	void setSpectrumToModel(int numBuckets);
	void setUniformColor(glm::vec4 color);
	glm::vec4 getColor() const;
	int getNumTriangles() const;
	Triangle* getTriangles() const;

	void scaleToOne();

protected:
	sf::Shader* shader;
	GLuint vao;
	GLuint positionBuffer;
	GLuint normalsBuffer;
	GLuint colorsBuffer;
	GLuint indexBuffer;
	GLuint numVerts;
	GLuint numTriangles;
	Triangle* triangles;

	glm::vec3 position;
	glm::vec3 scale;
	glm::vec4 color;
	glm::mat4 rotation;
	
	void addTriangle(Triangle& tri);
	void addTriangle(Vertex& a, Vertex& b, Vertex& c);
	void addTriangle(glm::vec3 a, glm::vec3 b, glm::vec3 c);
	void addQuad(Quad& quad);
	//void addQuad(Vertex& a, Vertex& b, Vertex& c, Vertex& d);
	void addQuad(glm::vec3 a, glm::vec3 b, glm::vec3 c, glm::vec3 d);

	void sendToGPU();
	void genFaceNormals();

// used to be private
	GLuint trianglesArraySize;
	bool usingColorAttrib;
	bool usingUniformColor;
	bool usingNormalAttrib;
	bool usingTextureAttrib;
	bool usingSpectrum;
	bool wireframe;
	static glm::vec3 newellMethod(glm::vec3 a, glm::vec3 b, glm::vec3 c);

private:
	void mixVert(Vertex *t, float min, float max, glm::vec3 start, glm::vec3 end);
	void mixIndex(Vertex *t, float min, float max, int numBuckets);
};

