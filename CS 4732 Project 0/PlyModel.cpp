#include "PlyModel.h"
#include <fstream>
#include <sstream>

using namespace std;

PlyModel::PlyModel(const char* fileName) : Mesh()
{
	ifstream file(fileName);
	char tmp[512];
	stringstream sstrm;
	glm::vec3* tmpVerts;
	GLuint numFaces;

	// check for Ply
	file.getline(tmp, 512);
	if (strncmp(tmp, "ply", 3) != 0)
	{
		cout << "Not a ply file: " << tmp << endl;
		exit(1);
	}

	// ignore format
	file.getline(tmp, 512);

	// read # vertices
	file.getline(tmp, 512);
	sstrm.str("");
	sstrm.clear();
	sstrm << tmp;
	sstrm >> tmp >> tmp >> numVerts;
	tmpVerts = new glm::vec3[numVerts];

	// skip next 3 lines
	for (int i = 0; i < 3; i++)
		file.getline(tmp, 512);

	// read # faces
	file.getline(tmp, 512);
	sstrm.str("");
	sstrm.clear();
	sstrm << tmp;
	sstrm >> tmp >> tmp >> numFaces;
										//skip next line
	file.getline(tmp, 512);

	// check for end header or skip
	file.getline(tmp, 512);

	// read vertices
	for (int i = 0; i < numVerts; i++)
	{
		file.getline(tmp, 512);
		sstrm.str("");
		sstrm.clear();
		sstrm << tmp;
		sstrm >> tmpVerts[i].x >> tmpVerts[i].y >> tmpVerts[i].z;
	}

	// read faces
	for (int i = 0; i < numFaces; i++)
	{
		int indices, a, b, c, d;
		file.getline(tmp, 512);

		sstrm.str("");
		sstrm.clear();
		sstrm << tmp;
		sstrm >> indices;
		if (indices == 3)
		{
			sstrm >> a >> b >> c;
			addTriangle(tmpVerts[a], tmpVerts[b], tmpVerts[c]);
		}
		else if (indices == 4)
		{
			sstrm >> a >> b >> c >> d;
			addQuad(tmpVerts[a], tmpVerts[b], tmpVerts[c], tmpVerts[d]);
		}
		//

	}

	genFaceNormals();
	sendToGPU();

	delete[] tmpVerts;
}

PlyModel::PlyModel(const char * fileName, int numBuckets) : Mesh()
{
	ifstream file(fileName);
	char tmp[512];
	stringstream sstrm;
	glm::vec3* tmpVerts;
	GLuint numFaces;

	// check for Ply
	file.getline(tmp, 512);
	if (strncmp(tmp, "ply", 3) != 0)
	{
		cout << "Not a ply file: " << tmp << endl;
		exit(1);
	}

	// ignore format
	file.getline(tmp, 512);

	// read # vertices
	file.getline(tmp, 512);
	sstrm.str("");
	sstrm.clear();
	sstrm << tmp;
	sstrm >> tmp >> tmp >> numVerts;
	tmpVerts = new glm::vec3[numVerts];

	// skip next 3 lines
	for (int i = 0; i < 3; i++)
		file.getline(tmp, 512);

	// read # faces
	file.getline(tmp, 512);
	sstrm.str("");
	sstrm.clear();
	sstrm << tmp;
	sstrm >> tmp >> tmp >> numFaces;
	//skip next line
	file.getline(tmp, 512);

	// check for end header or skip
	file.getline(tmp, 512);

	// read vertices
	for (int i = 0; i < numVerts; i++)
	{
		file.getline(tmp, 512);
		sstrm.str("");
		sstrm.clear();
		sstrm << tmp;
		sstrm >> tmpVerts[i].x >> tmpVerts[i].y >> tmpVerts[i].z;
	}

	// read faces
	for (int i = 0; i < numFaces; i++)
	{
		int indices, a, b, c, d;
		file.getline(tmp, 512);

		sstrm.str("");
		sstrm.clear();
		sstrm << tmp;
		sstrm >> indices;
		if (indices == 3)
		{
			sstrm >> a >> b >> c;
			addTriangle(tmpVerts[a], tmpVerts[b], tmpVerts[c]);
		}
		else if (indices == 4)
		{
			sstrm >> a >> b >> c >> d;
			addQuad(tmpVerts[a], tmpVerts[b], tmpVerts[c], tmpVerts[d]);
		}
		//

	}

	scaleToOne();
	genFaceNormals();
	setGradientOnX(glm::vec3(0.f, 1.f, 0.f), glm::vec3(0.f, 0.5f, 1.f));
	setSpectrumToModel(numBuckets);
	sendToGPU();

	delete[] tmpVerts;
}


PlyModel::~PlyModel()
{
}
