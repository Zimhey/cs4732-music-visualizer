/* Module     : Plane.h
* Author      : Corey Dixon
* Email       : cdixon@wpi.edu
* Course      : CS4732
*
* Description : Header file for the plane class. This class is a
* child of Mesh and generates the vertex data for a Plane
*
* Date        : 2017/4/07
*
* (c) Copyright 2017, Corey Dixon.
*/

#pragma once
#include "Mesh.h"
class Plane :
	public Mesh
{
public:
	Plane();
	Plane(int segments);
	~Plane();
};

