#pragma once

#include <GL/glew.h>
#include <SFML\Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <glm\vec3.hpp>
#include "Particle.h"
#include "MatrixStack.h"
#include "Mesh.h"

// TODO option to allow all particles to die, then kill the emitter
class Emitter
{
public:
	Emitter(int numParticles);
	Emitter(int numParticles, glm::vec3 position);
	~Emitter();

	// position
	void setPosition(glm::vec3 position);
	glm::vec3 getPosition() const;

	// velocity
	void setVelocity(glm::vec3 velocity);
	glm::vec3 getVelocity() const;

	// acceleration
	void setAcceleration(glm::vec3 acceleration);
	glm::vec3 getAcceleration() const;

	// spawn radius
	void setSpawnRadius(float radius);
	float getSpawnRadius() const;

	// shader
	void setShader(sf::Shader& shader);

	// life
	void setLife(float min, float max);
	void setMaxLife(float max);
	void setMinLife(float min);
	float getMaxLife() const;
	float getMinLife() const;

	// particle vel
	void setParticleVel(float min, float max);
	void setMaxParticleVel(float max);
	void setMinParticleVel(float min);
	void setParticleVelDir(const glm::vec3 dir);
	void setParticleVelConeAngle(float angle);
	float getMaxParticleVel() const;
	float getMinParticleVel() const;
	glm::vec3 getParticleVelDir() const;
	float getParticleVelConeAngle() const;

	// particle accel
	void setParticleAccel(float min, float max);
	void setMaxParticleAccel(float max);
	void setMinParticleAccel(float min);
	void setParticleAccelDir(const glm::vec3 dir);
	void setParticleAccelConeAngle(float angle);
	float getMaxParticleAccel() const;
	float getMinParticleAccel() const;
	glm::vec3 getParticleAccelDir() const;
	float getParticleAccelConeAngle() const;


	void init(Mesh* mesh);
	void update(float deltaTime);
	void draw(MatrixStack &stack);

private:
	int numParticles;
	Particle* particles;
	glm::vec3 position;
	glm::vec3 velocity;
	glm::vec3 acceleration;
	float spawnRadius;

	// opengl
	sf::Shader* shader;
	GLuint vao;
	GLuint meshBuffer;
	GLuint positionBuffer;
	GLuint numVerts;

	// particle
	float maxLife, minLife;
	// particle velocity
	float maxInitParticleVel, minInitParticleVel;
	glm::vec3 particleVelDir;
	float particleVelConeAngle;
	// particle acceleration
	float maxInitParticleAccel, minInitParticleAccel;
	glm::vec3 particleAccelDir;
	float particleAccelConeAngle;;

	Particle randomParticle();
	float randomFloat();
	glm::vec3 randomVecInCone(glm::vec3 axis, float angle);

};

