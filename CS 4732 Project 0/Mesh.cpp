/* Module     : Mesh.cpp
* Author      : Corey Dixon
* Email       : cdixon@wpi.edu
* Course      : CS4732
*
* Description : Implementation of the Mesh class. This class
* abstracts much of the OpenGL. The mesh can be given a
* position, rotation and scale. It provides functions for
* adding additional Triangles and Quads. It also has a
* generic draw function which updates the model matrices
* in the shader.
*
* Date        : 2017/03/20
*
* (c) Copyright 2017, Corey Dixon.
*/
#include "Mesh.h"
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/normal.hpp>

/**
* Constructs a Mesh
*/
Mesh::Mesh()
{
	trianglesArraySize = 2;
	triangles = new Triangle[trianglesArraySize];
	numVerts = 0;
	numTriangles = 0;
	wireframe = false;
	scale = { 1.f, 1.f, 1.f };
	position = { 0.f, 0.f, 0.f };
	//rotation = { 0.f, 0.f, 0.f };
	rotation = glm::mat4(1.f);
	usingColorAttrib = false;
	usingNormalAttrib = false;
	usingUniformColor = false;
	usingSpectrum = false;
}

/**
* Destructor
*/
Mesh::~Mesh()
{
	//if(triangles != NULL)
		//delete[] triangles; // deallocate
}

/**
* Draws the Mesh
*/
void Mesh::draw()
{
	glm::mat4 model = getModelViewMatrix();
	sf::Glsl::Mat4 mat = sf::Glsl::Mat4(&model[0][0]);
	shader->setUniform("model", mat);
	//if(usingUniformColor)
	//	shader->setUniformArray("color", &color[0], 4);
	sf::Shader::bind(shader);

	glBindVertexArray(vao);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	if (wireframe)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDrawArrays(GL_TRIANGLES, 0, numVerts);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
}

void Mesh::draw(MatrixStack &stack) 
{
	glm::mat4 model = stack.resultMat() * getModelViewMatrix();
	sf::Glsl::Mat4 mat = sf::Glsl::Mat4(&model[0][0]);
	shader->setUniform("model", mat);
	if (usingUniformColor)
	{
		sf::Glsl::Vec4 c(color.r, color.g, color.b, color.a);
		shader->setUniform("color", c);

	}
	sf::Shader::bind(shader);

	glBindVertexArray(vao);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	if(wireframe)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDrawArrays(GL_TRIANGLES, 0, numVerts);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
}

/**
* Position Mutator
*/
void Mesh::setPosition(glm::vec3 position)
{
	this->position = position;
}

/**
* Position Accessor
*/
glm::vec3 Mesh::getPosition() const
{
	return position;
}

/**
* Scale Mutator
*/
void Mesh::setScale(glm::vec3 scale)
{
	this->scale = scale;
}

/**
* Scale Accessor
*/
glm::vec3 Mesh::getScale() const
{
	return scale;
}

/**
* Rotation Mutator
*/
void Mesh::setRotation(glm::mat4 rotation)
{
	this->rotation = rotation;
}

/**
* Rotation Accessor
*/
glm::mat4 Mesh::getRotation() const
{
	return rotation;
}

/**
* Generates a Model to World Matrix
* 
* @return returns the ModelViewMatrix for use in the shader
*/
glm::mat4 Mesh::getModelViewMatrix() const
{
	glm::mat4 mat(1.f); // identity matrix
	mat = glm::translate(mat, position) * rotation * glm::scale(glm::mat4(1.f), scale);
	//mat = rotation* glm::translate(mat, position) * glm::scale(mat, scale);
	return mat;
}

/**
* Shader Mutator
*/
void Mesh::setShader(sf::Shader & shader)
{
	this->shader = &shader;
}

void Mesh::setWireFrame(bool useWireframe)
{
	this->wireframe = useWireframe;
}

void Mesh::setGradientOnX(glm::vec3 start, glm::vec3 end)
{
	// find min and max X
	float min = INFINITY;
	float max = -INFINITY;

	for (int i = 0; i < trianglesArraySize; i++)
	{
		// a
		if (triangles[i].a.position.x < min)
			min = triangles[i].a.position.x;
		if (triangles[i].a.position.x > max)
			max = triangles[i].a.position.x;
		// b
		if (triangles[i].b.position.x < min)
			min = triangles[i].b.position.x;
		if (triangles[i].b.position.x > max)
			max = triangles[i].b.position.x;
		// c
		if (triangles[i].c.position.x < min)
			min = triangles[i].c.position.x;
		if (triangles[i].c.position.x > max)
			max = triangles[i].c.position.x;
	}

	// mix between start and end based on the x position of each vertex

	for (int i = 0; i < trianglesArraySize; i++)
	{
		mixVert(&triangles[i].a, min, max, start, end);
		mixVert(&triangles[i].b, min, max, start, end);
		mixVert(&triangles[i].c, min, max, start, end);
	}
	this->usingColorAttrib = true;
}

void Mesh::setSpectrumToModel(int numBuckets)
{
	// find min and max X
	float min = INFINITY;
	float max = -INFINITY;

	for (int i = 0; i < trianglesArraySize; i++)
	{
		// a
		if (triangles[i].a.position.x < min)
			min = triangles[i].a.position.x;
		if (triangles[i].a.position.x > max)
			max = triangles[i].a.position.x;
		// b
		if (triangles[i].b.position.x < min)
			min = triangles[i].b.position.x;
		if (triangles[i].b.position.x > max)
			max = triangles[i].b.position.x;
		// c
		if (triangles[i].c.position.x < min)
			min = triangles[i].c.position.x;
		if (triangles[i].c.position.x > max)
			max = triangles[i].c.position.x;
	}

	// mix between start and end based on the x position of each vertex

	for (int i = 0; i < trianglesArraySize; i++)
	{
		mixIndex(&triangles[i].a, min, max, numBuckets);
		mixIndex(&triangles[i].b, min, max, numBuckets);
		mixIndex(&triangles[i].c, min, max, numBuckets);
	}
	this->usingSpectrum = true;

	sendToGPU();
}

void Mesh::setUniformColor(glm::vec4 color)
{
	this->color = color;
	this->usingUniformColor = true;
}

glm::vec4 Mesh::getColor() const
{
	return this->color;
}

int Mesh::getNumTriangles() const
{
	return numTriangles;
}

Triangle * Mesh::getTriangles() const
{
	return triangles;
}

void Mesh::scaleToOne()
{
	glm::vec3 min, max;

	min = triangles[0].a.position;
	max = triangles[0].a.position;

	for (int i = 0; i < numTriangles; i++)
	{
		// A
		// - x
		if (triangles[i].a.position.x < min.x)
			min.x = triangles[i].a.position.x;
		if (triangles[i].a.position.x > max.x)
			max.x = triangles[i].a.position.x;
		// - y
		if (triangles[i].a.position.y < min.y)
			min.y = triangles[i].a.position.y;
		if (triangles[i].a.position.y > max.y)
			max.y = triangles[i].a.position.y;
		// - z
		if (triangles[i].a.position.z < min.z)
			min.z = triangles[i].a.position.z;
		if (triangles[i].a.position.z > max.z)
			max.z = triangles[i].a.position.z;
		// B
		// - x
		if (triangles[i].b.position.x < min.x)
			min.x = triangles[i].b.position.x;
		if (triangles[i].b.position.x > max.x)
			max.x = triangles[i].b.position.x;
		// - y
		if (triangles[i].b.position.y < min.y)
			min.y = triangles[i].b.position.y;
		if (triangles[i].b.position.y > max.y)
			max.y = triangles[i].b.position.y;
		// - z
		if (triangles[i].b.position.z < min.z)
			min.z = triangles[i].b.position.z;
		if (triangles[i].b.position.z > max.z)
			max.z = triangles[i].b.position.z;

		// C
		// - x
		if (triangles[i].c.position.x < min.x)
			min.x = triangles[i].c.position.x;
		if (triangles[i].c.position.x > max.x)
			max.x = triangles[i].c.position.x;
		// - y
		if (triangles[i].c.position.y < min.y)
			min.y = triangles[i].c.position.y;
		if (triangles[i].c.position.y > max.y)
			max.y = triangles[i].c.position.y;
		// - z
		if (triangles[i].c.position.z < min.z)
			min.z = triangles[i].c.position.z;
		if (triangles[i].c.position.z > max.z)
			max.z = triangles[i].c.position.z;
	}

	float size = distance(min, max);
	float scale = 1 / size;
	glm::vec3 scaleVec = glm::vec3(scale, scale, scale);
	glm::mat3x3 scaleMat = (glm::mat3x3)glm::scale(scaleVec);

	for (int i = 0; i < numTriangles; i++)
	{
		Triangle t = triangles[i];
		triangles[i].a.position = scaleMat * t.a.position;
		triangles[i].b.position = scaleMat * t.b.position;
		triangles[i].c.position = scaleMat * t.c.position;
	}

}

/**
* Adds a triangle to the vertex data
* dynamically adjusts the array size
* @param tri The triangle to add
*/
void Mesh::addTriangle(Triangle & tri)
{
	if (numTriangles >= trianglesArraySize) 
	{
		Triangle* tmp = triangles;
		triangles = new Triangle[trianglesArraySize * 2];
		for (int i = 0; i < trianglesArraySize; i++)
		{
			triangles[i] = tmp[i];
		}
		trianglesArraySize *= 2;
		delete[] tmp;
	}

	triangles[numTriangles++] = tri;
	numVerts = numTriangles * 3;
}

/**
* Adds a triangle to the vertex data
* dynamically adjusts the array size
* @param a Vertex a of the triangle
* @param b Vertex b of the triangle
* @param c Vertex c of the triangle
*/
void Mesh::addTriangle(Vertex & a, Vertex & b, Vertex & c)
{
	Triangle tri;
	tri.a = a;
	tri.b = b;
	tri.c = c;
	addTriangle(tri);
}

void Mesh::addTriangle(glm::vec3 a, glm::vec3 b, glm::vec3 c)
{
	Triangle tri;
	tri.a.position = a;
	tri.b.position = b;
	tri.c.position = c;
	addTriangle(tri);
}

/**
* Adds a Quad to the vertex data
* dynamically adjusts the array size
* generates two triangles out of the quad
* @param quad The Quad to add
*/
void Mesh::addQuad(Quad & quad)
{
	Triangle one, two;
	one.a = quad.a;
	one.b = quad.b;
	one.c = quad.c;

	two.a = quad.c;
	two.b = quad.d;
	two.c = quad.a;

	addTriangle(one);
	addTriangle(two);
}

void Mesh::addQuad(glm::vec3 a, glm::vec3 b, glm::vec3 c, glm::vec3 d)
{
	Triangle tri;
	tri.a.position = a;
	tri.b.position = b;
	tri.c.position = c;
	addTriangle(tri);

	tri.a.position = c;
	tri.b.position = d;
	tri.c.position = a;
	addTriangle(tri);
}

/**
* Sends the Vertex data to the GPU
* TODO: allow arbitrary attributes
* TODO: only send new data to the GPU
* TODO: if replacing data, remove old data
*/
void Mesh::sendToGPU()
{
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glm::vec3* vertData;

	// position data
	glGenBuffers(1, &positionBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, positionBuffer);
	vertData = new glm::vec3[numVerts];
	for (int i = 0; i < numTriangles; i++)
	{
		vertData[i * 3] = triangles[i].a.position;
		vertData[i * 3 + 1] = triangles[i].b.position;
		vertData[i * 3 + 2] = triangles[i].c.position;
	}
	// copying vertex data over and enabling the position attribute
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * numVerts, glm::value_ptr(vertData[0]), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);

	if (usingNormalAttrib)
	{
		// normal data
		glGenBuffers(1, &normalsBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, normalsBuffer);
		for (int i = 0; i < numTriangles; i++)
		{
			vertData[i * 3] = triangles[i].a.normal;
			vertData[i * 3 + 1] = triangles[i].b.normal;
			vertData[i * 3 + 2] = triangles[i].c.normal;
		}
		// copying vertex data over and enabling the normal attribute
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * numVerts, glm::value_ptr(vertData[0]), GL_STATIC_DRAW);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, false, 0, 0);
	}

	if (usingColorAttrib)
	{
		// color data
		glGenBuffers(1, &colorsBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, colorsBuffer);
		for (int i = 0; i < numTriangles; i++)
		{
			vertData[i * 3] = triangles[i].a.color;
			vertData[i * 3 + 1] = triangles[i].b.color;
			vertData[i * 3 + 2] = triangles[i].c.color;
		}

		// copying vertex data over and enabling the color attribute
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * numVerts, glm::value_ptr(vertData[0]), GL_STATIC_DRAW);
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 3, GL_FLOAT, false, 0, 0);
	}

	// TODO move texture data to gpu

	if (usingSpectrum)
	{
		GLint* indices = new GLint[numVerts];
		glGenBuffers(1, &indexBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, indexBuffer);

		for (int i = 0; i < numTriangles; i++)
		{
			indices[i * 3] = triangles[i].a.index;
			indices[i * 3 + 1] = triangles[i].b.index;
			indices[i * 3 + 2] = triangles[i].c.index;
		}

		// copying vertex data over and enabling the index attribute
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLint) * numVerts, indices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(3);
		glVertexAttribIPointer(3, 1, GL_INT, 0, 0);
		delete[] indices;
	}


	delete[] vertData;
}

/**
* Generates face normals for all the triangles and sets the vertex attribute to on
*/
void Mesh::genFaceNormals()
{
	for (int i = 0; i < numTriangles; i++)
	{
		//glm::vec3 normal = newellMethod(triangles[i].a.position, triangles[i].b.position, triangles[i].c.position);
		glm::vec3 normal = triangleNormal(triangles[i].a.position, triangles[i].b.position, triangles[i].c.position);
		triangles[i].a.normal = normal;
		triangles[i].b.normal = normal;
		triangles[i].c.normal = normal;
	}
	usingNormalAttrib = true;
}

/**
* Calculate a normal for a triangle
*/
glm::vec3 Mesh::newellMethod(glm::vec3 a, glm::vec3 b, glm::vec3 c)
{
	/*
	glm::vec3 U = b - a;
	glm::vec3 V = c - a;

	glm::vec3 norm;
	norm.x = (U.y * V.z) - (U.z * V.y);
	norm.y = (U.z * V.x) - (U.x * V.z);
	norm.z = (U.x * V.y) - (U.y * V.x);
	norm = normalize(norm);
	
	return norm;
	*/
	glm::vec3 U = a - b;
	glm::vec3 V = c - b;
	return normalize(cross(U, V));
}

void Mesh::mixVert(Vertex* t, float min, float max, glm::vec3 start, glm::vec3 end)
{
	float len = max - min;
	t->color = mix(start, end, abs((min + t->position.x) / len));
}

void Mesh::mixIndex(Vertex * t, float min, float max, int numBuckets)
{
	float len = max - min; 
	glm::fvec1 fBuckets = glm::fvec1(numBuckets);
	glm::fvec1 zero = glm::fvec1(0.f);

	t->index = mix(zero, fBuckets, abs((min + t->position.x) / len)).x;
}
