#pragma once

#include <glm\vec3.hpp>

class Particle
{
public:
	Particle();
	Particle(float lifeTime);
	Particle(float lifeTime, const glm::vec3 position);
	Particle(float lifeTime, const glm::vec3 position, const glm::vec3 velocity);
	Particle(float lifeTime, const glm::vec3 position, const glm::vec3 velocity, const glm::vec3 acceleration);
	~Particle();

	float getLifeTime() const;
	void setLifeTime(float lifeTime);
	float getAge() const;
	void setAge(float age);
	glm::vec3 getPosition() const;
	void setPosition(const glm::vec3 position);
	glm::vec3 getVelocity() const;
	void setVelocity(const glm::vec3 velocity);
	glm::vec3 getAcceleration() const;
	void setAcceleration(const glm::vec3 acceleration);


	void update(float deltaTime);
	bool dead() const;

	float lifeTime;
	float age;
	glm::vec3 position;
	glm::vec3 velocity;
	glm::vec3 acceleration;
private:

};

