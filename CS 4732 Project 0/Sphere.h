/* Module     : Sphere.h
* Author      : Corey Dixon
* Email       : cdixon@wpi.edu
* Course      : CS4732
*
* Description : Header file for the Sphere class. This class is a
* child of Mesh and generates the vertex data for a Sphere
*
* Date        : 2017/03/20
*
* (c) Copyright 2017, Corey Dixon.
*/
#pragma once
#include "Mesh.h"
class Sphere :
	public Mesh
{
public:
	Sphere();
	Sphere(int numSegments);
	~Sphere();
private:
	Vertex genVert(int stack, int slice, int segments);
};

