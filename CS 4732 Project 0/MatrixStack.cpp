#include "MatrixStack.h"

MatrixStack::MatrixStack()
{
	numMats = 2;
	mats = new glm::mat4[numMats];
	used = 0;
}

MatrixStack::~MatrixStack()
{
	if (mats)
		delete[] mats;
}

void MatrixStack::push(glm::mat4 mat)
{
	if (used == numMats)
	{
		numMats *= 2;
		glm::mat4* tmp = mats;
		mats = new glm::mat4[numMats];
		for (int i = 0; i < used; i++)
			mats[i] = tmp[i];
		delete[] tmp;
	}
	mats[used++] = mat;
}

glm::mat4 MatrixStack::pop()
{
	if (!empty())
		return mats[--used];
	else
		return glm::mat4();
}


glm::mat4 MatrixStack::resultMat() const
{
	glm::mat4 result;
	//for (int i = used - 1; i >= 0; i--)
	for (int i = 0; i < used; i++)
		result *= mats[i];
	return result;
}

bool MatrixStack::empty() const
{
	return used == 0;
}
