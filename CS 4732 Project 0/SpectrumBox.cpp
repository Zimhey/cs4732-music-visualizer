#include "SpectrumBox.h"
#include <glm\gtx\transform.hpp>

SpectrumBox::SpectrumBox(int numBuckets) : Mesh()
{
	Vertex verts[8];
	float bounds = 0.5f; // create a cube from -0.5 to 0.5 (i.e. size 1)

						 // raw vert data
	verts[0].position = { -bounds, -bounds, -bounds };
	verts[1].position = { bounds, -bounds, -bounds };
	verts[2].position = { bounds, -bounds, bounds };
	verts[3].position = { -bounds, -bounds, bounds };

	// top
	verts[4].position = { -bounds, bounds, -bounds };
	verts[5].position = { bounds, bounds, -bounds };
	verts[6].position = { bounds, bounds, bounds };
	verts[7].position = { -bounds, bounds, bounds };

	// raw index data
	int cubeIndices[] = {
		0,3,2,1,
		2,3,7,6,
		0,4,7,3,
		1,2,6,5,
		4,5,6,7,
		0,1,5,4
	};

	// bottom has no spectrum
	verts[0].index = -1;
	verts[1].index = -1;
	verts[2].index = -1;
	verts[3].index = -1;

	// find left pos / start
	glm::vec3 start = -glm::vec3(1.f, 0.f, 0.f) * (numBuckets / 2.f);
	glm::vec3 width = glm::vec3(1.f, 0.f, 0.f) * bounds * 2.f;

	for (int j = 0; j < numBuckets; j++)
	{


		// set spectrum indices
		verts[4].index = j;
		verts[5].index = j;
		verts[6].index = j;
		verts[7].index = j;

		Quad quad;
		for (int i = 0; i < 6; i++) // generate quads for each side
		{
			quad.a = verts[cubeIndices[i * 4 + 0]];
			quad.b = verts[cubeIndices[i * 4 + 1]];
			quad.c = verts[cubeIndices[i * 4 + 2]];
			quad.d = verts[cubeIndices[i * 4 + 3]];

			// translate quad
			glm::mat4 t;
			t = glm::translate(start + width * (float)j);

			quad.a.position = t * glm::vec4(quad.a.position, 1.f);
			quad.b.position = t * glm::vec4(quad.b.position, 1.f);
			quad.c.position = t * glm::vec4(quad.c.position, 1.f);
			quad.d.position = t * glm::vec4(quad.d.position, 1.f);

			addQuad(quad);
		}
	}

	usingSpectrum = true;
	usingColorAttrib = true;

	setGradientOnX(glm::vec3(1.f, 1.f, 0.f), glm::vec3(1.f, 0.f, 1.f));

	genFaceNormals(); // generate face normals
	sendToGPU(); // update GPU
}


SpectrumBox::~SpectrumBox()
{
}
