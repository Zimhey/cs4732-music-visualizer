#include "SoundManager.h"



SoundManager::SoundManager()
{
	//Populate the list of songs here
	listOfSongNames = new char*[NUMSONGS];
	listOfSongNames[0] = "music/Ampersand.mp3";
	listOfSongNames[1] = "music/Colossus.mp3";
	listOfSongNames[2] = "music/Revelation.mp3";
	listOfSongNames[3] = "music/Meow.mp3";
	listOfSongNames[4] = "music/Starfish.mp3";
	listOfSongNames[5] = "music/warcraft.mp3";

	listOfSongs = new Sound*[NUMSONGS];
	playlistIndex = 0;

	//initialize fmod
	System_Create(&fmodSys);
	fmodSys->init(2, FMOD_INIT_NORMAL, NULL);
	fmodSys->getMasterChannelGroup(&channelGroup);
	fmodSys->createDSPByType(FMOD_DSP_TYPE_FFT, &fft);
	fft->setParameterInt(FMOD_DSP_FFT_WINDOWTYPE, FMOD_DSP_FFT_WINDOW_RECT);
	channelGroup->addDSP(FMOD_CHANNELCONTROL_DSP_HEAD, fft);

	//Create streams for each song
	for (int i = 0; i < NUMSONGS; i++)
	{
		fmodSys->createStream(listOfSongNames[i], FMOD_DEFAULT, NULL, &listOfSongs[i]);
	}

}


SoundManager::~SoundManager()
{
	delete[] listOfSongs;
	delete[] listOfSongNames;
}

void SoundManager::Play()
{
	fmodSys->playSound(listOfSongs[playlistIndex], NULL, false, &channel);
}

void SoundManager::PlayNext()
{
	//Stop song
	StopSong();
	//increment index
	playlistIndex++;
	if (playlistIndex > NUMSONGS) {
		playlistIndex = 0;
	}
	//play song
	Play();
}

void SoundManager::TogglePause(bool isPaused)
{
	channel->setPaused(isPaused);
}

void SoundManager::PlayPrev()
{
	//stop song
	StopSong();
	//increment index
	playlistIndex--;
	if (playlistIndex < 0) {
		playlistIndex = NUMSONGS;
	}
	//play song
	Play();
}

void SoundManager::RestartSong()
{
	channel->setPosition(0, FMOD_TIMEUNIT_PCM);
}

void SoundManager::StopSong()
{
	channel->stop();
}

Sound ** SoundManager::GetSongList()
{
	return listOfSongs;
}
