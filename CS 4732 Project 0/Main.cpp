/* Module     : Main.cpp
* Author      : Corey Dixon
* Email       : cdixon@wpi.edu
* Course      : CS4732
*
* Description : Uses SFML to create a window and add an OpenGL context to it.
* Additionally handles SFML events including keyboard events. Pushing Esc
* will exit.
*
* Date        : 2017/03/16
*
* (c) Copyright 2017, Corey Dixon.
*/

#include <GL/glew.h>
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
#include <SFML/Graphics/Shader.hpp>
#include "Application.h"


int main()
{
	// Application class to manage our scene
	Application* app;
	bool fullscreen = true;

	// Request a 24-bits depth buffer when creating the window
	sf::ContextSettings contextSettings;
	contextSettings.depthBits = 24;

	sf::VideoMode resWindowed(1280, 720);
	sf::VideoMode resFull(1920, 1080);
	sf::VideoMode res4k(3840, 2160);

	// Create the main window
	sf::Window window(resFull, "Corey Dixon & Marco Duran: CS4732 Final Project", sf::Style::Default, contextSettings);
	// Make it the active window for OpenGL calls
	window.setActive();

	// Configure the viewport (the same size as the window)
	glViewport(0, 0, window.getSize().x, window.getSize().y);

	// Create a clock for measuring the time elapsed
	sf::Clock clock;

	glewInit();
	app = new Application(&window);
	app->initShaders();
	app->initScene();
	app->initCamera();
	
	// Start the game loop
	while (window.isOpen())
	{
		/*
		// Process events
		sf::Event event;
		while (window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == sf::Event::Closed)
				window.close();

			// Escape key: exit
			if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Escape))
				window.close();

			if ((event.type == sf::Event::KeyPressed) &&  (event.key.code == sf::Keyboard::P))
				app->pause = !app->pause;

			// Resize event: adjust the viewport
			if (event.type == sf::Event::Resized)
				glViewport(0, 0, event.size.width, event.size.height);
		}
		*/
		// Draw the cube
		app->updateScene();
		app->drawScene();

		// Finally, display the rendered frame on screen
		window.display();
	}

	return EXIT_SUCCESS;
}
