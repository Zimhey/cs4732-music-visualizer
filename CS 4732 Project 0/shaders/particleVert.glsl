#version 430

uniform mat4 perspective;
uniform mat4 camera;
uniform mat4 model;

in layout(location = 0) vec4 vPosition;
in layout(location = 1) vec3 particlePosition;

out vec4 color;

void main() 
{
	mat4 endMat = perspective * camera * model;
	gl_Position = endMat * (vPosition + vec4(particlePosition, 0.f));
	color = vec4(particlePosition, 1.f);
} 
