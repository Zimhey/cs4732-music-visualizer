/* Module     : Application.h
* Author      : Corey Dixon
* Email       : cdixon@wpi.edu
* Course      : CS4732
*
* Description : Header file for the Application class. This class handles
* loading a basic shader, initializing the scene, drawing the scene and
* updating the scene.
*
* Date        : 2017/03/16
*
* (c) Copyright 2017, Corey Dixon.
*/

#pragma once

#include<SFML\Graphics\Shader.hpp>
#include <glm/matrix.hpp>
#include <vector>
#include "Cube.h"
#include "Sphere.h"
#include "Plane.h"
#include "PlyModel.h"
#include "Emitter.h"
#include "SoundManager.h"
//#include <fmod.h>
//#include <fmod_errors.h>
#include <fmod.hpp>
#include "SpectrumBox.h"
#include "SpectrumSphere.h"

using namespace std;

class Application
{
public:
	Application(sf::Window* window);
	~Application();
	void initShaders();
	void initScene();
	void initCamera();
	void drawScene();
	void calcSpectrum();
	void updateScene();


	bool pause;

private:
	sf::Window* window;
	sf::Shader spectrumShader;
	sf::Shader basicShader;
	sf::Shader normalsShader;
	glm::mat4 perspective;
	glm::mat4 camera;

	sf::Clock loopTime;
	sf::Clock appTime;
	float totalTime;

	//Camera
	glm::vec3 cameraPos;
	glm::vec3 cameraDir;
	glm::vec3 cameraTarget;
	glm::vec3 vecUp = { 0.f, 1.f, 0.f };
	glm::vec3 cameraRight;
	glm::vec3 cameraUp;
	glm::vec3 cameraFront;
	GLfloat yaw = 0.f, pitch = 0.f;
	bool firstMouse = true;
	GLint lastX = 0.f, lastY = 0.f;
	GLfloat xOffset = 0.f, yOffset = 0.f;
	GLfloat sensitivity = 0.075f * 0.75f;
	glm::vec3 newFront = { 0.f, 0.f, 0.f };

	Plane* spectrumPlane;
	Plane* plane;
	SpectrumBox* spectrumBox;
	SpectrumSphere* spectrumSphere;
	Sphere* aSphere;
	PlyModel* model;
	PlyModel** models;


	//Adding in sound manager
	/*
	// fmod
	FMOD::System *fmodSys;
	FMOD::Sound *music;
	FMOD::Channel *channel;
	FMOD::ChannelGroup *channelGroup;
	FMOD::DSP *fft;

	const int SPECTRUM_SIZE = 512;
	const int NUM_BUCKETS = 256;
	const float NYQUIST_FREQ = 22050.f;
	const float START_FREQ = 20.f;

	const float BIN_FREQ_RANGE = NYQUIST_FREQ / (SPECTRUM_SIZE * 2);
	// http://www.wolframalpha.com/input/?i=20*(1+%2B+x)+%5Ey%3D+22000+solve+x
	const float scale = pow((NYQUIST_FREQ / START_FREQ), 1.f / NUM_BUCKETS); // x axis generator
	*/
	SoundManager *soundManager;
	float* spectrumArray;


	glm::vec3 randomPosition() const;

};

