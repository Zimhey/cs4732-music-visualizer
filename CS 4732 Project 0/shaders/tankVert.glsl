#version 430

uniform mat4 perspective;
uniform mat4 camera;
uniform mat4 model;

in layout(location = 0) vec4 vPosition;
in layout(location = 1) vec4 normal;

void main() 
{
	mat4 endMat = perspective * camera * model;
	gl_Position = endMat * vPosition;
	//gl_Position = endMat * (vPosition + vec4(0.f, waveHeight * sin(time * 3 + vPosition.x * 6), 0.f, 0.f));
	//vec4 shift = vPosition + vec4(0.f, 3.f, 0.f, 0.f);
	//gl_Position = endMat * vPosition;
} 
